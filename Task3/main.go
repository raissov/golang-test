package main

import (
	"fmt"
	"log"
	"net/http"
	"runtime"
)

const windowsConst = "windows os run"
const linuxConst = "program run on linux"
const plan9Const = "this is plan9"

func main() {
	// Пакет runtime содержит себе константу GOOS которая тоже меняется в зависимости от OS.
	//Ниже мы выводим соответствующий результат на консоль
	switch os := runtime.GOOS; os {
	case "windows":
		fmt.Println(windowsConst)
	case "linux":
		fmt.Println(linuxConst)
	case "plan9":
		fmt.Println(plan9Const)
	}
	// А тут мы показываем сообщение об OS уже в браузере перейдя по ссылке localhost:8080/checkOS
	http.HandleFunc("/checkOS", func(w http.ResponseWriter, r *http.Request) {
		switch os := runtime.GOOS; os {
		case "windows":
			fmt.Fprintf(w, windowsConst)
		case "linux":
			fmt.Fprintf(w, linuxConst)
		case "plan9":
			fmt.Fprintf(w, plan9Const)
		}
	})
	log.Fatal(http.ListenAndServe(":8081", nil))
}
